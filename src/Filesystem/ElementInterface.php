<?php

namespace Mars\Filesystem;

interface ElementInterface
{
    public function exists();
    public function open();
    public function close();
}