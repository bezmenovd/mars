<?php

namespace Mars\Filesystem;

use Mars\Support\Collection;
use Mars\Debug\Exception;
use Mars\Filesystem\Element;
use Mars\Filesystem\ElementInterface;
use Mars\Filesystem\File;

class Directory extends Element implements ElementInterface
{
    /**
     * @return bool
     */
    public function exists()
    {
        return is_dir($this->path);
    }

    /**
     * @return Collection
     */
    public function files()
    {
        $list = $this->scan();

        $files = new Collection();

        foreach ($list as $path)
        {
            if (is_file($path))
                $files->push(new File($path));
        }

        return $files;
    }

    /**
     * @return Collection
     */
    public function directories()
    {
        $list = $this->scan();
        $dirs = new Collection();

        foreach ($list as $path)
        {
            if (is_dir($path))
                $dirs->push(new Directory($path));
        }

        return $files;
    }

    /**
     * @param bool $clear Without a parent and current directories' items
     * 
     * @return array
     */
    public function scan(bool $clear = true)
    {
        $this->ensureIsOpen();

        $list = [];

        while (false !== ($entry = $this->read()))
            $list[] = $entry;

        if ($clear)
            $list = array_diff($list, ['..', '.']);

        return $list;
    }

    /**
     * @return string
     */
    public function read()
    {
        $this->ensureIsOpen();

        return readdir($this->handle);
    }

    /**
     * @return Directory
     */
    public function open()
    {
        $this->ensureExists();

        $this->isOpen = true;

        $this->handle = opendir($this->path);

        return $this;
    }

    /**
     * @return Directory
     */
    public function close()
    {
        $this->ensureIsOpen();

        closedir($this->handle);

        $this->handle = null;
        $this->isOpen = false;

        return $this;
    }
}