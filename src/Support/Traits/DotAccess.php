<?php

namespace Mars\Support\Traits;

trait DotAccess
{
    /**
     * @var array $array
     */
    protected $array;

    /**
     * Get value by name
     * 
     * @param string $name
     */
    public function get(string $name)
    {
        $parts = explode('.', $name);
        $target = $this->array;

        foreach ($parts as $part)
        {
            if (mb_strlen($part) == 0)
                continue;
                
            if (key_exists($part, $target))
                $target = $target[$part];
            else
                return null;
        }

        return $target;
    }

    /**
     * Set value by name
     * 
     * @param string $name
     * @param mixed  $value
     */
    public function set(string $name, $value)
    {
        $parts = explode('.', $name);
        $target = &$this->array;

        foreach ($parts as $part)
        {
            if (mb_strlen($part) == 0)
                continue;

            if (key_exists($part, $target))
                $target = &$target[$part];
            else
                return false;
        }

        $target = $value;

        return true;
    }
}