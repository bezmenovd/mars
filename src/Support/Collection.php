<?php

namespace Mars\Support;

class Collection
{
    /**
     * @var array $array;
     */
    protected $array;

    /**
     * @param array $array
     */
    public function __construct(array $array = [])
    {
        $this->array = $array;
    }

    /**
     * @return array
     */
    public function get(string $key = "")
    {
        if (empty($key))
            return $this->array;
        else
            if (key_exists($key, $this->array))
                return $this->array[$key];
            else
                return null;
    }

    /**
     * @param string $key
     * @param mixed  $value
     */
    public function set(string $key, $value)
    {
        $this->array[$key] = $value;
    }

    /**
     * @param string $key
     * @param mixed  $value
     */
    public function add(string $key, $value)
    {
        $this->array[$key][] = $value;
    }

    /**
     * @param mixed $value
     */
    public function push($value)
    {
        $this->arrayp[] = $value;
    }

    /**
     * Remove value from collection
     * 
     * @param mixed $value
     * @param bool  $strict Strict comparison
     */
    public function remove($value, bool $strict = true)
    {
        if ($strict)
            $callback = function($item) use ($value) {
                return $item === $value;
            };
        else
            $callback = function($item) use ($value) {
                return $item == $value;
            };

        return $this->filter($callback);
    }

    /**
     * @return Collection
     */
    public function keys()
    {
        return new static(array_keys($this->array));
    }

    /**
     * @return Collection
     */
    public function values()
    {
        return new static(array_values($this->array));
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->array);
    }

    /**
     * @return bool
     */
    public function isAssociative()
    {
        return $this->values() == $this->array;
    }

    /**
     * Make associative array using value of field in multidimensional array as key
     * 
     * @param string $field    Name of field
     * @param bool   $override Override item at new index if it already exists, else make array of items
     */
    public function makeAssociative(string $field, bool $override = true)
    {
        $result = $this->array;

        foreach ($result as $index => $item)
        {
            $value = $item[$field];
            
            unset($item[$field]);
            unset($result[$index]);

            if (!isset($result[$value]) || $override)
                $result[$value] = $item;
            else
                $result[$value][] = $item;
        }

        return new static($result);
    }

    /**
     * @param callable $callback
     * 
     * @return Collection
     */
    public function filter(callable $callback)
    {
        return new static(array_filter($this->array, $callback));
    }

    /**
     * @param callable $callback
     * 
     * @return Collection
     */
    public function map(callale $callback)
    {
        return new static(array_map($callback, $this->array));
    }

    /**
     * @param callable $callback
     * @param mixed    $start    Start value
     * 
     * @return Collection
     */
    public function reduce(callale $callback, $start)
    {
        return new static(array_reduce($this->array, $callback, $start));
    }

    /**
     * @param mixed $condition Simple value, array of key => values for searching in multidimensional array, callable
     * 
     * @return Collection
     */
    public function search($condition)
    {
        if (is_callable($condition))
        {
            return $this->filter($condition);
        }
        else if (is_array($condition))
        {
            $condition = new Collection($condition);

            if ($condition->isAssociative())
            {
                $func = function($item)
                {
                    foreach ($condition->get() as $key => $value)
                    {
                        if ($item[$key] !== $value)
                            return false;
                    }

                    return true;
                };
            }
            else
            {
                $func = function($item)
                {
                    return in_array($item, $condition);
                };
            }

            return $this->filter($func);
        }
        else
        {
            return $this->filter(function($item)
            {
                return $item === $condition;
            });
        }
    }

    /**
     * @param mixed $condition Simple value, array of key => values for searching in multidimensional array, callable
     * 
     * @return bool
     */
    public function contains($condition)
    {
        return $this->search($condition)->count() > 0;
    }

    /**
     * Iterate of array items
     * 
     * @param callable $callback
     */
    public function forEach(callable $callback)
    {
        foreach ($this->array as $index => $item)
            $callback($item, $index);

        return $this;
    }

    /**
     * Iterate of array items at their removal
     * 
     * @param callable $callback
     */
    public function finalEach(callable $callback)
    {
        foreach ($this->array as $index => $item)
        {
            unset($this->array[$index]);
            $callback($item);
        }

        return $this;
    }
}