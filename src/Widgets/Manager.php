<?php

namespace Mars\Widgets;

use Mars\Widgets\Widget;

class Manager
{
    /**
     * @var string $paths
     */
    protected static $paths;
    
    /**
     * @param string $name   Widget name
     * @param array  $params Array of widget parameters
     * @param string $body   Inner content
     */
    public static function include(string $name, array $params = [], string $body = "")
    {
        return static::getWidget($name)->include($params, $body);
    }
    
    /**
     * @param string $name Name of the widget like <name_part1>.<name_part2>:<theme>
     **/
    public static function getWidget(string $name)
    {
        [$name, $theme] = explode(":", $name);

        $found = false;

        /**
         * Search widget in paths
         */
        foreach ((array)static::$paths as $dir)
        {
            $widget = new Widget($name, static::$dir);
            
            if ($widget->exists())
            {
                $found = true;
                break;
            }
        }

        if (!$found)
            throw new \Exception("Widget ($name) not found in current paths");
        
        if ($theme)
            $widget->setTheme($theme);

        return $widget;
    }

    /**
     * @param string $path Relative path to the directory containing widgets
     */
    public static function addPath(string $path)
    {
        if (!file_exists($_SERVER["DOCUMENT_ROOT"] . "/" . ltrim($path, "/")))
            throw new \Exception("Directory does not exist");

        static::$paths[] = $path;
    }
}