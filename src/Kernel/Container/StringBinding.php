<?php

namespace Mars\Kernel\Container;

use Mars\Kernel\Container\Binding;

class StringBinding implements Binding
{
    /**
     * @var string $binding;
     */
    protected $binding;

    /**
     * @param string $binding
     */
    public function __construct(string $binding)
    {
        $this->binding = $binding;
    }

    public static function isValidFactoryItem($binding)
    {
        return is_string($binding);
    }

    public function get(...$params)
    {
        return $this->binding;
    }
}