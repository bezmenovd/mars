<?php

namespace Mars\Kernel;

use Mars\Support\Collection;
use Mars\Support\Factory;
use Mars\Kernel\KernelException;
use Mars\Kernel\Container;

class Container
{
    /**
     * Contains classes and objects
     * 
     * @var Collection $container
     */
    protected $container;

    /**
     * @var Factory $factory
     */
    protected $factory;

    /**
     * 
     */
    public function __construct()
    {
        $this->factory = new Factory([
            Container\StringBinding::class,
            Container\CallbackBinding::class,
        ]);
    }

    /**
     * Bind name in container
     * 
     * @param string $name
     * @param mixed  $binding
     */
    public function bind(string $name, $binding)
    {
        if ($this->container->contains($name))
            throw new KernelException("Container name is busy: $name");

        if (!($bindingClass = $this->factory->search($binding)))
            throw new KernelException("Invalid binding type");

        $this->container->set($name, new $bindingClass($binding));
    }

    /**
     * Get item from container
     * 
     * @param string $name
     */
    public function get(string $name, ...$params)
    {
        if (!$this->container->contains($name))
            throw new KernelException("Access to unbinded container item: $name");

        $binding = $this->container->get($name);

        return $binding->get(...$params);
    }
}